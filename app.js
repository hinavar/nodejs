
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');

var connection = require('./config');
var router = express.Router();

var app = express();

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/static',express.static('public'));
app.set('view engine', 'ejs')

app.use('/',require('./routes/routes'));


app.get('/home', function (request, response) {
    if (request.session.loggedin) {
        response.send('Welcome, ' + request.session.username + '! <br><a href="/logout">logout</a> <a href="/setting">change password</a>');
    } else {
        response.send('Please <a href="/">login</a> to view this page!');
    }
    response.end();
});


app.get('/logout', function (req, res) {
    req.session.destroy(function (err) {
        if (err) {
            console.log(err);
        }
        else {
            res.redirect('/');
        }
    });

});



app.listen(3001);
var express = require('express');
var session = require('express-session');

var connection = require('../config');
var validateController = require('./loginController');


exports.Register=function (request, response){
 



    var email = request.body.email;
    var pass = request.body.pass;
    var user = request.body.user;

if (user && email && pass) { 
    connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [email, pass], function (error, results, fields) {
        if (results.length > 0) {
            response.send('Existed Username and/or Password!');
        }
        else if (!validateEmail(email)) {
           response.send('Wrong Email');
        }
        else {
            connection.query('INSERT INTO accounts (username, password,email) VALUES (?,?,?)', [user, pass, email], function (err, res) {
                if (err) console.log(err);
                console.log("1 record inserted");
            });
            request.session.loggedin = true;
            request.session.username = user;
            response.redirect('/home');
        }
        response.end();
    });

}


};

function validateEmail(email) {

    var emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
    var valid = emailRegex.test(email);
    if (!valid)
        return false;
    else return true;
}
var express = require('express');
var router = express.Router();
var path = require('path');

var registerController = require('../controllers/registerController');
var loginController = require('../controllers/loginController');
var settingController = require('../controllers/settingController');



router.get('/signup', function (request, response) {
    response.render('signup');
    
});

router.get('/', function (request, response) {
    response.render('login');
});

router.get('/setting', function (request, response) {
    response.render('setting');
});


router.post('/controllers/registerController',registerController.Register);

router.post('/controllers/loginController',loginController.Login);

router.post('/controllers/settingController',settingController.setting);


module.exports=router;